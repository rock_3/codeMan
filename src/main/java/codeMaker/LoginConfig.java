package codeMaker;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.Toolkit;

import javax.swing.DefaultComboBoxModel;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.JTextField;
import javax.swing.LayoutStyle.ComponentPlacement;

import constant.ChildWindowConstant;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class LoginConfig {

	private JFrame frame;
	private JTextField user1;
	private JTextField pwd1;
	private JTextField pwd2;
	private JTextField user2;
	private JTextField user3;
	private JTextField pwd3;
	private JTextField userTable;
	private JTextField userNameFiled;
	private JTextField pwdFiled;


	public JFrame getFrame() {
		return frame;
	}

	/**
	 * Launch the application.
	 */
	public static void main() {
		EventQueue.invokeLater(() -> {
			try {
				LoginConfig window = new LoginConfig();
				ChildWindowConstant.loginConfig = window;
				window.frame.setVisible(true);
			} catch (Exception ignored) {
			}
		});
	}

	/**
	 * Create the application.
	 */
	private LoginConfig() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setResizable(false);
		frame.setTitle("登录配置");
		frame.setIconImage(Toolkit.getDefaultToolkit().getImage(LoginConfig.class.getResource(
				"/org/pushingpixels/substance/internal/contrib/randelshofer/quaqua/images/color_wheel.png")));
		frame.setBounds(100, 100, 759, 420);
		frame.setLocationRelativeTo(null);
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);

		JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		tabbedPane.setBorder(null);

		JLabel label = new JLabel("界面选择");

		JComboBox<String> selectStyle = new JComboBox<>();
		selectStyle.setModel(new DefaultComboBoxModel<>(new String[]{"default"}));

		JButton saveConfig = new JButton("保存配置");

		GroupLayout groupLayout = new GroupLayout(frame.getContentPane());
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.TRAILING)
				.addGroup(groupLayout.createSequentialGroup()
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addGroup(groupLayout.createSequentialGroup()
							.addContainerGap()
							.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
								.addComponent(tabbedPane, GroupLayout.DEFAULT_SIZE, 705, Short.MAX_VALUE)
								.addGroup(groupLayout.createSequentialGroup()
									.addComponent(label)
									.addPreferredGap(ComponentPlacement.UNRELATED)
									.addComponent(selectStyle, GroupLayout.PREFERRED_SIZE, 112, GroupLayout.PREFERRED_SIZE))))
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(260)
							.addComponent(saveConfig)))
					.addContainerGap())
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addContainerGap()
					.addComponent(tabbedPane, GroupLayout.PREFERRED_SIZE, 204, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(label)
						.addComponent(selectStyle, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(29)
					.addComponent(saveConfig)
					.addContainerGap(75, Short.MAX_VALUE))
		);

		JPanel panel = new JPanel();
		tabbedPane.addTab("静态用户（不查询数据库）", null, panel, null);

		JLabel label_1 = new JLabel("用户1");
		label_1.setFont(new Font("宋体", Font.PLAIN, 15));

		JLabel label_2 = new JLabel("密码");
		label_2.setFont(new Font("宋体", Font.PLAIN, 15));

		user1 = new JTextField();
		user1.setColumns(10);

		pwd1 = new JTextField();
		pwd1.setColumns(10);

		JLabel label_3 = new JLabel("用户2");
		label_3.setFont(new Font("宋体", Font.PLAIN, 15));

		JLabel label_4 = new JLabel("密码");
		label_4.setFont(new Font("宋体", Font.PLAIN, 15));

		pwd2 = new JTextField();
		pwd2.setColumns(10);

		user2 = new JTextField();
		user2.setColumns(10);

		JLabel label_5 = new JLabel("用户3");
		label_5.setFont(new Font("宋体", Font.PLAIN, 15));

		JLabel label_6 = new JLabel("密码");
		label_6.setFont(new Font("宋体", Font.PLAIN, 15));

		user3 = new JTextField();
		user3.setColumns(10);

		pwd3 = new JTextField();
		pwd3.setColumns(10);

		JLabel lbladminroot = new JLabel("最多配置三个用户！生成代码后可以用其中之一进行登录，如果不配置默认用户为admin，密码为root");
		GroupLayout gl_panel = new GroupLayout(panel);
		gl_panel.setHorizontalGroup(
				gl_panel.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_panel.createSequentialGroup().addContainerGap().addGroup(gl_panel
								.createParallelGroup(Alignment.LEADING).addGroup(gl_panel.createSequentialGroup()
										.addGroup(gl_panel.createParallelGroup(Alignment.LEADING)
												.addGroup(gl_panel.createSequentialGroup().addComponent(label_1)
														.addPreferredGap(ComponentPlacement.RELATED).addComponent(user1,
																GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
																GroupLayout.PREFERRED_SIZE))
												.addGroup(gl_panel.createSequentialGroup().addComponent(label_3)
														.addPreferredGap(ComponentPlacement.RELATED).addComponent(user2,
																GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
																GroupLayout.PREFERRED_SIZE))
												.addGroup(gl_panel.createSequentialGroup().addComponent(label_5)
														.addPreferredGap(ComponentPlacement.RELATED)
														.addComponent(user3, GroupLayout.PREFERRED_SIZE,
																GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
										.addGap(32)
										.addGroup(gl_panel.createParallelGroup(Alignment.LEADING)
												.addGroup(gl_panel.createSequentialGroup().addComponent(label_6)
														.addPreferredGap(ComponentPlacement.RELATED)
														.addComponent(pwd3, GroupLayout.PREFERRED_SIZE,
																GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
												.addGroup(gl_panel.createSequentialGroup().addComponent(label_2)
														.addPreferredGap(ComponentPlacement.RELATED)
														.addComponent(pwd1, GroupLayout.PREFERRED_SIZE,
																GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
												.addGroup(gl_panel.createSequentialGroup().addComponent(label_4)
														.addPreferredGap(ComponentPlacement.RELATED).addComponent(pwd2,
																GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
																GroupLayout.PREFERRED_SIZE))))
								.addComponent(lbladminroot)).addContainerGap(326, Short.MAX_VALUE)));
		gl_panel.setVerticalGroup(gl_panel.createParallelGroup(Alignment.LEADING).addGroup(gl_panel
				.createSequentialGroup().addContainerGap()
				.addGroup(gl_panel.createParallelGroup(Alignment.BASELINE).addComponent(label_1).addComponent(label_2)
						.addComponent(user1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
								GroupLayout.PREFERRED_SIZE)
						.addComponent(pwd1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
								GroupLayout.PREFERRED_SIZE))
				.addGap(18)
				.addGroup(gl_panel.createParallelGroup(Alignment.BASELINE).addComponent(label_3).addComponent(label_4)
						.addComponent(user2, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
								GroupLayout.PREFERRED_SIZE)
						.addComponent(pwd2, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
								GroupLayout.PREFERRED_SIZE))
				.addGap(18)
				.addGroup(gl_panel.createParallelGroup(Alignment.BASELINE).addComponent(label_5).addComponent(label_6)
						.addComponent(user3, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
								GroupLayout.PREFERRED_SIZE)
						.addComponent(pwd3, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
								GroupLayout.PREFERRED_SIZE))
				.addPreferredGap(ComponentPlacement.RELATED, 27, Short.MAX_VALUE).addComponent(lbladminroot)
				.addGap(24)));
		panel.setLayout(gl_panel);
		tabbedPane.setBackgroundAt(0, Color.WHITE);

		JPanel panel1 = new JPanel();
		tabbedPane.addTab("动态用户（查询数据库）", null, panel1, null);

		JLabel label7 = new JLabel("用户表");
		label7.setFont(new Font("宋体", Font.PLAIN, 15));

		userTable = new JTextField();
		userTable.setColumns(10);

		JLabel label8 = new JLabel("用户名字段");
		label8.setFont(new Font("宋体", Font.PLAIN, 15));

		userNameFiled = new JTextField();
		userNameFiled.setColumns(10);

		JLabel label9 = new JLabel("密码字段");
		label9.setFont(new Font("宋体", Font.PLAIN, 15));

		pwdFiled = new JTextField();
		pwdFiled.setColumns(10);

		JLabel lblsql = new JLabel(
				"<html><body>请填写数据库中用户表的名称以及对应的用户名和密码字段的英文名称便于动态生成sql语句<br/><br/>这里只是考虑最简单的情况，如果有加密等处理，生成后自行添加相应逻辑即可<br/></body</html>");
		GroupLayout gl_panel_1 = new GroupLayout(panel1);
		gl_panel_1.setHorizontalGroup(gl_panel_1.createParallelGroup(Alignment.LEADING).addGroup(gl_panel_1
				.createSequentialGroup().addContainerGap()
				.addGroup(gl_panel_1.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_panel_1.createParallelGroup(Alignment.TRAILING)
								.addGroup(gl_panel_1.createSequentialGroup().addComponent(label7)
										.addPreferredGap(ComponentPlacement.RELATED)
										.addComponent(userTable, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
												GroupLayout.PREFERRED_SIZE)
										.addPreferredGap(ComponentPlacement.RELATED))
								.addGroup(Alignment.LEADING,
										gl_panel_1.createSequentialGroup().addComponent(label8)
												.addPreferredGap(ComponentPlacement.RELATED).addComponent(userNameFiled,
														GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
														GroupLayout.PREFERRED_SIZE)))
						.addGroup(gl_panel_1.createSequentialGroup().addGap(14).addComponent(label9)
								.addPreferredGap(ComponentPlacement.RELATED)
								.addComponent(pwdFiled, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
										GroupLayout.PREFERRED_SIZE)))
				.addGap(18).addComponent(lblsql).addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)));
		gl_panel_1
				.setVerticalGroup(gl_panel_1.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_panel_1.createSequentialGroup().addGroup(gl_panel_1
								.createParallelGroup(Alignment.TRAILING, false)
								.addGroup(Alignment.LEADING, gl_panel_1
										.createSequentialGroup().addGap(28).addComponent(lblsql,
												GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
								.addGroup(Alignment.LEADING,
										gl_panel_1.createSequentialGroup().addContainerGap()
												.addGroup(gl_panel_1.createParallelGroup(Alignment.BASELINE)
														.addComponent(label7)
														.addComponent(userTable, GroupLayout.PREFERRED_SIZE,
																GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
												.addGap(18)
												.addGroup(gl_panel_1
														.createParallelGroup(Alignment.BASELINE).addComponent(label8)
														.addComponent(userNameFiled, GroupLayout.PREFERRED_SIZE,
																GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
												.addGap(18)
												.addGroup(gl_panel_1.createParallelGroup(Alignment.BASELINE)
														.addComponent(label9).addComponent(pwdFiled,
																GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
																GroupLayout.PREFERRED_SIZE))))
								.addContainerGap(66, Short.MAX_VALUE)));
		panel1.setLayout(gl_panel_1);

		if (ChildWindowConstant.user1.size() != 0) {
			user1.setText(ChildWindowConstant.user1.get(0));
			pwd1.setText(ChildWindowConstant.user1.get(1));
		}
		if (ChildWindowConstant.user2.size() != 0) {
			user2.setText(ChildWindowConstant.user2.get(0));
			pwd2.setText(ChildWindowConstant.user2.get(1));
		}
		if (ChildWindowConstant.user3.size() != 0) {
			user3.setText(ChildWindowConstant.user3.get(0));
			pwd3.setText(ChildWindowConstant.user3.get(1));
		}

		if (ChildWindowConstant.dynamicUserList.size() != 0) {

			userTable.setText(ChildWindowConstant.dynamicUserList.get(0));
			userNameFiled.setText(ChildWindowConstant.dynamicUserList.get(1));
			pwdFiled.setText(ChildWindowConstant.dynamicUserList.get(2));
		}

		saveConfig.addActionListener(e -> {

			String user1Name = user1.getText();
			user1.setText(user1Name);
			String pwd1Value = pwd1.getText();
			pwd1.setText(pwd1Value);

			String user2Name = user2.getText();
			user2.setText(user2Name);
			String pwd2Value = pwd2.getText();
			pwd2.setText(pwd2Value);

			String user3Name = user3.getText();
			user3.setText(user3Name);
			String pwd3Value = pwd3.getText();
			pwd3.setText(pwd3Value);

			if (!"".equals(user1Name)) {
				ChildWindowConstant.user1.clear();
				ChildWindowConstant.user1.add(user1Name);
				ChildWindowConstant.user1.add(pwd1Value);
			}

			if (!"".equals(user2Name)) {
				ChildWindowConstant.user2.clear();
				ChildWindowConstant.user2.add(user2Name);
				ChildWindowConstant.user2.add(pwd2Value);
			}

			if (!"".equals(user3Name)) {
				ChildWindowConstant.user3.clear();
				ChildWindowConstant.user3.add(user3Name);
				ChildWindowConstant.user3.add(pwd3Value);
			}

			String userTableText = userTable.getText();
			userTable.setText(userTableText);
			String userNameText = userNameFiled.getText();
			userNameFiled.setText(userNameText);
			String pwdText = pwdFiled.getText();
			pwdFiled.setText(pwdText);
			if (!"".equals(userTableText) && !"".equals(userNameText) && !"".equals(pwdText)) {
				ChildWindowConstant.dynamicUserList.clear();
				ChildWindowConstant.dynamicUserList.add(userTableText);
				ChildWindowConstant.dynamicUserList.add(userNameText);
				ChildWindowConstant.dynamicUserList.add(pwdText);
			}

			if (ChildWindowConstant.user1.size() == 0 && ChildWindowConstant.user2.size() == 0
					&& ChildWindowConstant.user3.size() == 0) {
				ChildWindowConstant.user1.add("admin");
				ChildWindowConstant.user1.add("root");
			}

			JOptionPane.showMessageDialog(frame, "配置成功！", "提示", JOptionPane.INFORMATION_MESSAGE);

		});

		frame.getContentPane().setLayout(groupLayout);
	}
}
