package constant;

import util.CodeWriterUtil;

import javax.swing.JFrame;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;

public class Constant {

	public static JFrame frmv;

	public static final String PROJECT_NAME = "freeOut";

	public static final String ZIP_PWD = "beautiful123,,#@";

	public static final String FREE_OUT_PATH = "" + File.separator + "freeOut" + File.separator + "";

	public static final String FREE_OUT_PATH_LIB_MVC = "" + File.separator + "freeOut" + File.separator + "src" + File.separator + "main" + File.separator + "webapp" + File.separator + "WEB-INF" + File.separator + "lib" + File.separator + "";

	public static final String FREE_OUT_PATH_LIB = "" + File.separator + "freeOut" + File.separator + "lib" + File.separator + "";

	public static final String FREE_OUT_PATH_METAINF_MVC = "" + File.separator + "freeOut" + File.separator + "src" + File.separator + "main" + File.separator + "webapp" + File.separator + "META-INF" + File.separator + "";

	public static final String MAIN_APPLICATION_PATH = "" + File.separator + "freeOut" + File.separator + "src" + File.separator + "main" + File.separator + "java" + File.separator + "freeOut" + File.separator + "";

	public static final String TEST_PATH = "" + File.separator + "freeOut" + File.separator + "src" + File.separator + "test" + File.separator + "java" + File.separator + "freeOut" + File.separator + "";

	public static final String FREE_OUT_PATH_CONTROLLER = "" + File.separator + "freeOut" + File.separator + "src" + File.separator + "main" + File.separator + "java" + File.separator + "freeOut" + File.separator + "controller" + File.separator + "";

	public static final String FREE_OUT_PATH_CORE = "" + File.separator + "freeOut" + File.separator + "src" + File.separator + "main" + File.separator + "java" + File.separator + "freeOut" + File.separator + "core" + File.separator + "";

	public static final String FREE_OUT_PATH_CORE_ANNOTATION = "" + File.separator + "freeOut" + File.separator + "src" + File.separator + "main" + File.separator + "java" + File.separator + "freeOut" + File.separator + "core" + File.separator + "annotation" + File.separator + "";

	public static final String FREE_OUT_PATH_CORE_VALIDATES = "" + File.separator + "freeOut" + File.separator + "src" + File.separator + "main" + File.separator + "java" + File.separator + "freeOut" + File.separator + "core" + File.separator + "validates" + File.separator + "";

	public static final String FREE_OUT_PATH_DTO = "" + File.separator + "freeOut" + File.separator + "src" + File.separator + "main" + File.separator + "java" + File.separator + "freeOut" + File.separator + "dto" + File.separator;

	public static final String FREE_OUT_PATH_CORE_EXCEPTION = "" + File.separator + "freeOut" + File.separator + "src" + File.separator + "main" + File.separator + "java" + File.separator + "freeOut" + File.separator + "core" + File.separator + "exception" + File.separator + "";

	public static final String FREE_OUT_PATH_SERVICE = "" + File.separator + "freeOut" + File.separator + "src" + File.separator + "main" + File.separator + "java" + File.separator + "freeOut" + File.separator + "service" + File.separator + "";

	public static final String FREE_OUT_PATH_SERVICE_IMPL = "" + File.separator + "freeOut" + File.separator + "src" + File.separator + "main" + File.separator + "java" + File.separator + "freeOut" + File.separator + "service" + File.separator + "impl" + File.separator + "";

	public static final String FREE_OUT_PATH_DAO = "" + File.separator + "freeOut" + File.separator + "src" + File.separator + "main" + File.separator + "java" + File.separator + "freeOut" + File.separator + "dao" + File.separator + "";

	public static final String FREE_OUT_PATH_ENTITY = "" + File.separator + "freeOut" + File.separator + "src" + File.separator + "main" + File.separator + "java" + File.separator + "freeOut" + File.separator + "entity" + File.separator + "";

	public static final String FREE_OUT_PATH_CONSTANT = "" + File.separator + "freeOut" + File.separator + "src" + File.separator + "main" + File.separator + "java" + File.separator + "freeOut" + File.separator + "constant" + File.separator + "";

	public static final String FREE_OUT_PATH_RESOURCES = "" + File.separator + "freeOut" + File.separator + "src" + File.separator + "main" + File.separator + "resources" + File.separator + "";

	public static final String FREE_OUT_PATH_SQL_MAPPER = "" + File.separator + "freeOut" + File.separator + "src" + File.separator + "main" + File.separator + "resources" + File.separator + "sqlmapper" + File.separator + "";

	public static final String FREE_OUT_PATH_SQL_MAPPER_MVC = "" + File.separator + "freeOut" + File.separator + "src" + File.separator + "main" + File.separator + "java" + File.separator + "freeOut" + File.separator + "sqlmapper" + File.separator + "";

	public static final String FREE_OUT_PATH_STATIC = "" + File.separator + "freeOut" + File.separator + "src" + File.separator + "main" + File.separator + "resources" + File.separator + "static" + File.separator + "mystatic" + File.separator + "";

	public static final String FREE_OUT_PATH_STATIC_MVC = "" + File.separator + "freeOut" + File.separator + "src" + File.separator + "main" + File.separator + "webapp" + File.separator + "mystatic" + File.separator + "";

	public static final String FREE_OUT_PATH_TEMPLATES = "" + File.separator + "freeOut" + File.separator + "src" + File.separator + "main" + File.separator + "resources" + File.separator + "templates" + File.separator + "";

	public static final String FREE_OUT_PATH_WEBINF_MVC = "" + File.separator + "freeOut" + File.separator + "src" + File.separator + "main" + File.separator + "webapp" + File.separator + "WEB-INF" + File.separator + "";

	public static final String FREE_OUT_PATH_SETTINGS = "" + File.separator + "freeOut" + File.separator + ".settings" + File.separator + "";

	public static final String UTIL_PATH = "" + File.separator + "freeOut" + File.separator + "src" + File.separator + "main" + File.separator + "java" + File.separator + "freeOut" + File.separator + "utils" + File.separator + "";

	public static final String MVC_CONFIG = "" + File.separator + "freeOut" + File.separator + "src" + File.separator + "main" + File.separator + "java" + File.separator + "freeOut" + File.separator + "config" + File.separator + "";

	public static final String MUTI_DATA_SOURCE = MVC_CONFIG + "mutidatasource" + File.separator + "";

	public static final String EVERYTHING_DOWN_LOAD_DIR = "C:" + File.separator + "codeManConfig" + File.separator + "everything" + File.separator + "";

	public static final String EVERYTHING_EXE_NAME = "C:" + File.separator + "codeManConfig" + File.separator + "everything" + File.separator + "Everything.exe";

	public static final String WEB_CLIENT_PATH = "" + File.separator + "freeOut" + File.separator + "src" + File.separator + "main" + File.separator + "resources" + File.separator + "static" + File.separator + "";

	public static final String WEB_CLIENT_PATH_MVC = "" + File.separator + "freeOut" + File.separator + "src" + File.separator + "main" + File.separator + "webapp" + File.separator + "";

	public static final String WEB_CLIENT_HTML_PATH = WEB_CLIENT_PATH + "views" + File.separator + "";

	public static final String WEB_CLIENT_HTML_PATH_MVC = WEB_CLIENT_PATH_MVC + "views" + File.separator + "";

	public static final String WEB_CLIENT_JS_CONFIG_PATH = WEB_CLIENT_PATH + "js" + File.separator + "config" + File.separator + "";

	public static final String WEB_CLIENT_JS_CONFIG_PATH_MVC = WEB_CLIENT_PATH_MVC + "js" + File.separator + "config" + File.separator + "";

	public static final String MAC_OS = "Mac OS";

	public static final String WINDOWS = "Windows";
	//基础
	public static final String CLOUD_BASE = File.separator + "freeOut" + File.separator;
	//父工程
	public static final String CLOUD_SYS_PARENT = "sys-parent" + File.separator;
	//eureka
	public static final String SYS_EUREKA = "sys-eureka" + File.separator;
	public static final String CLOUD_SYS_EUREKA = "sys-eureka" + File.separator + "src" + File.separator + "main" + File.separator + "java" + File.separator + "eureka" + File.separator;
	public static final String CLOUD_SYS_EUREKA_RESOURCES = "sys-eureka" + File.separator + "src" + File.separator + "main" + File.separator + "resources" + File.separator;
	//zuul
	public static final String SYS_ZUUL = "sys-zuul" + File.separator;
	public static final String CLOUD_SYS_ZUUL = "sys-zuul" + File.separator + "src" + File.separator + "main" + File.separator + "java" + File.separator + "zuul" + File.separator;
	public static final String CLOUD_SYS_ZUUL_CONFIG = CLOUD_SYS_ZUUL + "config" + File.separator;
	public static final String CLOUD_SYS_ZUUL_RESOURCES = "sys-zuul" + File.separator + "src" + File.separator + "main" + File.separator + "resources" + File.separator;
	//gateway
	public static final String SYS_GATEWAY = "sys-gateway" + File.separator;
	public static final String CLOUD_SYS_GATEWAY = "sys-gateway" + File.separator + "src" + File.separator + "main" + File.separator + "java" + File.separator + "gateway" + File.separator;
	public static final String CLOUD_SYS_GATEWAY_RESOURCES = "sys-gateway" + File.separator + "src" + File.separator + "main" + File.separator + "resources" + File.separator;
	//sys-common
	public static final String CLOUD_SYS_COMMON = "sys-common" + File.separator;
	//service-system
	public static final String CLOUD_SERVICE_SYSTEM = "service-system" + File.separator;
	public static final String CLOUD_SERVICE_SYSTEM_MAIN = "service-system" + File.separator + "src" + File.separator + "main" + File.separator + "java" + File.separator + "freeOut" + File.separator;
	public static final String CLOUD_SERVICE_SYSTEM_RESOURCES = "service-system" + File.separator + "src" + File.separator + "main" + File.separator + "resources" + File.separator;

	public static final String CLOUD_FEIGN = "sys-feign-api" + File.separator + "src" + File.separator + "main" + File.separator + "java" + File.separator + "feign" + File.separator;
	public static final String CLOUD_FEIGN_CLIENT = CLOUD_FEIGN + "clients" + File.separator + "freeOut" + File.separator;
	public static final String CLOUD_FEIGN_CONFIG = CLOUD_FEIGN + File.separator + "config" + File.separator;
	public static final String CLOUD_FEIGN_BASE = "sys-feign-api" + File.separator;

	public static final String FREE_OUT_PATH_INIT = "" + File.separator + "freeOut" + File.separator + "src" + File.separator + "main" + File.separator + "java" + File.separator + "freeOut" + File.separator + "init" + File.separator + "";

	public static String lifePropertiesPath = "C:" + File.separator + "codeManConfig" + File.separator + "codeManSdk" + File.separator + "bin" + File.separator + "server" + File.separator + "life.properties";

	public static String modelFiles = "C:" + File.separator + "codeManConfig" + File.separator + "modelFiles" + File.separator + "";

	public static String modelFilesZip = "C:" + File.separator + "codeManConfig" + File.separator + "modelFiles" + File.separator + "model.zip";

	public static String staticZip = "C:" + File.separator + "codeManConfig" + File.separator + "modelFiles" + File.separator + "static.zip";

	public static String webClientZip = "C:" + File.separator + "codeManConfig" + File.separator + "modelFiles" + File.separator + "webClient.zip";

	public static String mvcSettingsZip = "C:" + File.separator + "codeManConfig" + File.separator + "modelFiles" + File.separator + "mvcSettings.zip";

	public static String settingsZip = "C:" + File.separator + "codeManConfig" + File.separator + "modelFiles" + File.separator + "settings.zip";

	public static String libZip = "C:" + File.separator + "codeManConfig" + File.separator + "modelFiles" + File.separator + "lib.zip";

	public static String downLibZipNet = "/zip/lib.zip";

	public static String downSettingsZipNet = "/zip/settings.zip";

	public static String downMvcSettingsZipNet = "/zip/mvcSettings.zip";

	private static String downStaticZipNet = "/zip/static.zip";

	private static String downWebClientZipNet = "/zip/webClient.zip";

	public static String everythingZip = "/zip/everything.zip";

	public static String dubboModelZip = "/zip/dubboModel.zip";

	public static String cloudModelZip = "/zip/cloudModel.zip";

	public static String gitIpAdressFilePath = "C:" + File.separator + "codeManConfig" + File.separator + "codeManIp" + File.separator + "ip.txt";

	public static String gitConfigFilePath = "C:" + File.separator + "codeManConfig" + File.separator + ".git";

	public static String parametersPath = "C:" + File.separator + "codeManConfig" + File.separator + "config" + File.separator + "parameters.bl";

	public static String tableConfigPath = "C:" + File.separator + "codeManConfig" + File.separator + "config" + File.separator + "tableConfig.bl";


	static {
		//如果是mac系统
		if (Constant.MAC_OS.equals(CodeWriterUtil.getSystemType())) {
			modelFiles = "/Applications/codeManConfig/modelFiles/";
			modelFilesZip = "/Applications/codeManConfig/modelFiles/model.zip";
			staticZip = "/Applications/codeManConfig/modelFiles/static.zip";
			webClientZip = "/Applications/codeManConfig/modelFiles/webClient.zip";
			mvcSettingsZip = "/Applications/codeManConfig/modelFiles/mvcSettings.zip";
			settingsZip = "/Applications/codeManConfig/modelFiles/settings.zip";
			libZip = "/Applications/codeManConfig/modelFiles/lib.zip";
			lifePropertiesPath = "/Applications/codeManConfig/codeManSdk/Home/life.properties";
			gitIpAdressFilePath = "/Applications/codeManConfig/codeManIp/ip.txt";
			gitConfigFilePath = "/Applications/codeManConfig/.git";
			parametersPath = "/Applications/codeManConfig/parameters.bl";
			tableConfigPath = "/Applications/codeManConfig/tableConfig.bl";
		}
		File ipFile = new File(Constant.gitIpAdressFilePath);
		try (BufferedReader reader = new BufferedReader(new FileReader(ipFile))) {
			String ipPath = reader.readLine();
			Constant.downSettingsZipNet = ipPath + Constant.downSettingsZipNet;
			Constant.downMvcSettingsZipNet = ipPath + Constant.downMvcSettingsZipNet;
			Constant.downStaticZipNet = ipPath + Constant.downStaticZipNet;
			Constant.downWebClientZipNet = ipPath + Constant.downWebClientZipNet;
			Constant.downLibZipNet = ipPath + Constant.downLibZipNet;
			Constant.everythingZip = ipPath + Constant.everythingZip;
			Constant.dubboModelZip = ipPath + Constant.dubboModelZip;
			Constant.cloudModelZip = ipPath + Constant.cloudModelZip;
		} catch (Exception ignored) {
		}
	}


}
