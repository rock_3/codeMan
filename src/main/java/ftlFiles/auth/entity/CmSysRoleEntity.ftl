package <#if isCloudModel>${cloudSysEngName}<#else>${packageName}</#if>.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import <#if isCloudModel>${cloudSysEngName}<#else>${packageName}</#if>.core.annotation.ExternalField;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

@ApiModel
@Data
@EqualsAndHashCode(callSuper = false)
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class CmSysRoleEntity extends CommonEntity implements Serializable {

    /**
     * serialVersionUID
     */
    private static final long serialVersionUID = 1L;

    /**
	 *  role_id
	 */
	@ApiModelProperty(value = "role_id", name = "roleId")
    private Long roleId;
    /**
	 *  role_name
	 */
	@ApiModelProperty(value = "role_name", name = "roleName")
    private String roleName;
    /**
	 *  create_time
	 */
	@ApiModelProperty(value = "create_time", name = "createTime")
    private Date createTime;
    /**
	 *  update_time
	 */
	@ApiModelProperty(value = "update_time", name = "updateTime")
    private Date updateTime;
    /**
	 *  create_user_id
	 */
	@ApiModelProperty(value = "create_user_id", name = "createUserId")
    private Long createUserId;
    /**
	 *  update_user_id
	 */
	@ApiModelProperty(value = "update_user_id", name = "updateUserId")
    private Long updateUserId;

	@ApiModelProperty("角色所拥有的的菜单列表（包括按钮）")
	@ExternalField
	private List<CmSysMenuEntity> cmSysMenuEntityList;

}
