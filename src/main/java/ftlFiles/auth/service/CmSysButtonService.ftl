package <#if isCloudModel>${cloudSysEngName}<#else>${packageName}</#if>.service;

import <#if isCloudModel>${cloudSysEngName}<#else>${packageName}</#if>.entity.CmSysButtonEntity;

public interface CmSysButtonService {

	void add(CmSysButtonEntity entity);

	void delete(CmSysButtonEntity entity);

	void update(CmSysButtonEntity entity);

}
