package <#if isCloudModel>${cloudSysEngName}<#else>${packageName}</#if>.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import <#if isCloudModel>${cloudSysEngName}<#else>${packageName}</#if>.constant.MoveType;
import <#if isCloudModel>${cloudSysEngName}<#else>${packageName}</#if>.constant.YesOrNo;
import <#if isCloudModel>${cloudSysEngName}<#else>${packageName}</#if>.core.exception.BusinessException;
import <#if isCloudModel>${cloudSysEngName}<#else>${packageName}</#if>.dao.CmSysMenuDao;
import <#if isCloudModel>${cloudSysEngName}<#else>${packageName}</#if>.dto.NodeMoveDto;
import <#if isCloudModel>${cloudSysEngName}<#else>${packageName}</#if>.entity.CmSysButtonEntity;
import <#if isCloudModel>${cloudSysEngName}<#else>${packageName}</#if>.entity.CmSysMenuEntity;
import <#if isCloudModel>${cloudSysEngName}<#else>${packageName}</#if>.service.CmSysMenuService;
import <#if isCloudModel>${cloudSysEngName}<#else>${packageName}</#if>.utils.SnowflakeIdWorker;

import java.awt.image.BufferStrategy;
import java.util.ArrayList;
import java.util.List;

@Service
public class CmSysMenuServiceImpl implements CmSysMenuService {


	private final CmSysMenuDao dao;

	@Autowired
	public CmSysMenuServiceImpl(CmSysMenuDao dao) {
		this.dao = dao;
	}

	@Override
	public void add(CmSysMenuEntity entity) {
		dao.add(entity);
	}

	@Override
	public void delete(CmSysMenuEntity entity) {
		if (YesOrNo.NO.getCode().equals(entity.getCanDel())) {
			throw new BusinessException("基础菜单不允许删除！");
		}
		dao.delete(entity);
		//同步删除子菜单及按钮
		dao.deleteChildMenu(entity.getMenuId());
		dao.deleteChildButton(entity.getMenuId());
		//删除关联的权限
		dao.delRoleMenuNotReal();
		dao.delRoleButtonNotReal();
	}

	@Override
	public void update(CmSysMenuEntity entity) {
		dao.update(entity);
	}

	@Override
	public List<CmSysMenuEntity> listAll(Long roleId, YesOrNo isAll) {
		List<CmSysMenuEntity> cmSysMenuEntities = dao.listAll(roleId, isAll.getCode());
		List<CmSysMenuEntity> buttons = new ArrayList<>(10);
		for (CmSysMenuEntity menuEntity : cmSysMenuEntities) {
			menuEntity.setIsButton(YesOrNo.NO.getCode());
			//根据菜单查按钮
			List<CmSysButtonEntity> buttonEntities = dao.getButtonsByMenuId(menuEntity.getMenuId(), roleId, isAll.getCode());
			//添加到buttons列表
			for (CmSysButtonEntity cmSysButtonEntity : buttonEntities) {
				CmSysMenuEntity buttonMenu = new CmSysMenuEntity();
				buttonMenu.setMenuId(cmSysButtonEntity.getButtonId());
				buttonMenu.setParentId(cmSysButtonEntity.getMenuId());
				buttonMenu.setName(cmSysButtonEntity.getButtonName());
				buttonMenu.setCanDel(cmSysButtonEntity.getCanDel());
				buttonMenu.setIsButton(YesOrNo.YES.getCode());
				buttonMenu.setModuleTagId(cmSysButtonEntity.getModuleTagId());
				buttonMenu.setRoleId(cmSysButtonEntity.getRoleId());
				buttons.add(buttonMenu);
			}
		}
		cmSysMenuEntities.addAll(buttons);
		return cmSysMenuEntities;
	}

	@Override
	public CmSysMenuEntity removeNode(NodeMoveDto nodeMoveDto) {
		CmSysMenuEntity moveMenu = nodeMoveDto.getMoveMenu();
		CmSysMenuEntity targetMenu = nodeMoveDto.getTargetMenu();
		MoveType moveType = nodeMoveDto.getMoveType();
		Long moveMenuNo = moveMenu.getOrderNo();
		Long targetMenuNo = targetMenu.getOrderNo();
		//如果是移动到节点内部，更新moveMenu的parentId即可
		if (MoveType.inner.equals(moveType)) {
			Long menuId = targetMenu.getMenuId();
			moveMenu.setParentId(menuId);
			moveMenu.setOrderNo(SnowflakeIdWorker.generateId());
			update(moveMenu);
			//如果是移动到上方或下方，交换两者的orderNo即可（因为只会在同一个parentId下移动）
		} else {
			moveMenu.setOrderNo(targetMenuNo);
			update(moveMenu);
			targetMenu.setOrderNo(moveMenuNo);
			update(targetMenu);
		}
		return moveMenu;
	}

	@Override
	public boolean canMove(NodeMoveDto nodeMoveDto) {
		CmSysMenuEntity moveMenu = nodeMoveDto.getMoveMenu();
		CmSysMenuEntity targetMenu = nodeMoveDto.getTargetMenu();
		if (targetMenu.getOrderNo() >= moveMenu.getOrderNo()) {
			return false;
		}
		return dao.countByOrderNo(targetMenu.getOrderNo(), moveMenu.getOrderNo(), moveMenu.getParentId()) <= 0;
	}
}
