<!DOCTYPE html>
<#if theme == "经典后台Thymleaf版">
<HTML lang="en" xmlns="http://www.w3.org/1999/xhtml" xmlns:th="http://www.thymeleaf.org">
<HEAD>
    <TITLE>菜单信息管理</TITLE>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <link rel="stylesheet" th:href="@{/mystatic/bootstrap/css/bootstrap.min-custom.css}">
    <link rel="stylesheet" th:href="@{/mystatic/progressbar/css/mprogress.css}">
    <link rel="stylesheet" th:href="@{/mystatic/ztree/css/bootstrapztree.css}" type="text/css">
    <link rel="stylesheet" th:href="@{/mystatic/ztree/css/personality.css}" type="text/css">
    <script th:src="@{/mystatic/ztree/js/jquery.min.js}"></script>
    <script th:src="@{/mystatic/ztree/js/jquery.ztree.core.js}"></script>
    <script th:src="@{/mystatic/ztree/js/jquery.ztree.excheck.js}"></script>
    <script th:src="@{/mystatic/ztree/js/jquery.ztree.exedit.js}"></script>
    <script th:src="@{/mystatic/bootstrap/js/bootstrap.min.js}"></script>
    <script th:src="@{/mystatic/js/ajaxFactory.js}"></script>
    <script th:src="@{/mystatic/js/jqAlert.js}"></script>
    <script th:src="@{/mystatic/progressbar/js/mprogress.js}"></script>
    <script th:src="@{/mystatic/progressbar/js/init-mprogress.js}"></script>
    <script th:src="@{/mystatic/js/mustache/mustache.min.js}"></script>
    <script th:src="@{/mystatic/js/config.js}"></script>
    <#else>
    <HTML lang="en">
    <HEAD>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>菜单信息管理</title>
        <meta name="keywords" content="">
        <meta name="description" content="">
        <link rel="shortcut icon" href="../favicon.ico">
        <link href="../../css/bootstrap.min-custom.css" rel="stylesheet">
        <link href="../../css/font-awesome.css?v=4.4.0" rel="stylesheet">
        <link href="../../css/plugins/bootstrap-table/bootstrap-table.min.css" rel="stylesheet">
        <link href="../../css/animate.css" rel="stylesheet">
        <link href="../../css/style.css?v=4.1.0" rel="stylesheet">
        <link href="../../css/plugins/pageMe/pageMe.css" rel="stylesheet"/>
        <link href="../../css/plugins/progressbar/mprogress.css" rel="stylesheet"/>
        <link href="../../css/plugins/progressbar/style.css" rel="stylesheet"/>
        <link rel="stylesheet" href="../../js/ztree/css/bootstrapztree.css" type="text/css">
        <link rel="stylesheet" href="../../js/ztree/css/personality.css" type="text/css">
        <script src="../../js/ztree/js/jquery.min.js"></script>
        <script src="../../js/ztree/js/jquery.ztree.core.js"></script>
        <script src="../../js/ztree/js/jquery.ztree.excheck.js"></script>
        <script src="../../js/ztree/js/jquery.ztree.exedit.js"></script>
        <script src="../../js/bootstrap.min.js"></script>
        <script src="../../js/util/ajaxFactory.js"></script>
        <script src="../../js/plugins/layer/layer.min.js"></script>
        <script src="../../js/plugins/progressbar/init-mprogress.js"></script>
        <script src="../../js/plugins/progressbar/mprogress.js"></script>
        <script src="../../js/plugins/mustache/mustache.js"></script>
        <script src="../../js/config/config.js"></script>
        </#if>

        <SCRIPT type="text/javascript">
            var controllerPrefix = "<#if isCloudModel>system-service/</#if>cmSysMenu";
            var nowUpTreeNode;
            //当前添加节点时候的父类id 根节点为0
            var nowParentId = 0;
            //当前添加节点时候父类对象 根节点为null
            var nowParentNode = null;
            var isAddButton = false;
            var setting = {
                view: {
                    addDiyDom: addDiyDom,
                    selectedMulti: false,
                    //不显示连线
                    showLine: false
                },
                callback: {
                    onClick: zTreeOnClick,
                    onDrop: zTreeOnDrop
                },
                check: {
                    enable: false
                },
                data: {
                    simpleData: {
                        enable: true,
                        idKey: "menuId",
                        pIdKey: "parentId",
                        rootPId: 0
                    }
                },
                edit: {
                    enable: true,
                    showRemoveBtn: false,
                    showRenameBtn: false,
                    drag: {
                        prev: canPreNext,
                        next: canPreNext,
                        inner: canInner
                    }
                }
            };

            var zNodes = [
                /*{menuId: "12345678901234567890", parentId: 0, menuName: "[core] 基本功能 演示", open: true, urlAddress: null},
                {menuId: 101, parentId: "12345678901234567890", name: "最简单的树 --  标准 JSON 数据", urlAddress: "123"},*/
            ];

            $(document).ready(function () {
                initTree();
            });

            function initTree() {
                var userInfo = JSON.parse(sessionStorage.getItem(USER_INFO_KEY));
                //动态获取树形菜单
                $z.ajaxGet({
                    url: basePath + "/" + controllerPrefix + "/list-all",
                    async: false,
                    data: {
                        roleId: userInfo.roleId,
                        isAll: "YES"
                    },
                    beforeSend: function () {
                        InitMprogress();
                    },
                    success: function (data) {
                        $z.dealCommonResult(data, function () {
                            //赋值
                            zNodes = data.data;
                            for (var i in zNodes) {
                                if (zNodes[i].isButton === 0) {
                                    zNodes[i].open = true;
                                }
                            }
                            //初始化
                            $.fn.zTree.init($("#ztree"), setting, zNodes);
                            MprogressEnd();
                            showButtonByRole();
                        });
                    }
                });
            }

            function canPreNext(treeId, nodes, targetNode) {
                var checkFlag = nodes[0].level === targetNode.level && nodes[0].parentId === targetNode.parentId && nodes[0].level !== 2 && targetNode.level !== 2
                if (targetNode && checkFlag) {
                    var canMove = false;
                    $z.ajaxPost({
                        url:  basePath + "/" + controllerPrefix + "/can-move",
                        async: false,
                        data: {
                            moveMenu: nodes[0],
                            targetMenu: targetNode,
                        },
                        success: function (data) {
                            canMove = data.data;
                        }
                    });
                    return checkFlag && canMove;
                }
                return false;
            }

            function canInner(treeId, nodes, targetNode) {
                //只能往根节点里拖
                if (targetNode && nodes[0].level !== 2 && nodes[0].level > targetNode.level) {
                    return true;
                }
                return false;
            }

            /**
             * 拖拽触发的函数
             */
            function zTreeOnDrop(event, treeId, treeNodes, targetNode, moveType) {
                if (!moveType) {
                    return;
                }
                //请求移动节点接口
                $z.ajaxPost({
                    url: basePath + "/" + controllerPrefix + "/remove-node",
                    data: {
                        moveMenu: treeNodes[0],
                        targetMenu: targetNode,
                        moveType: moveType
                    },
                    beforeSend: function () {
                        InitMprogress();
                    },
                    success: function (data) {
                        $z.dealCommonResult(data, function () {
                            var treeObj = $.fn.zTree.getZTreeObj("ztree");
                            //同步更新
                            if (moveType == "inner") {
                                //更新被移动节点的parentId
                                treeNodes[0].parentId = targetNode.menuId;
                                treeNodes[0].orderNo = data.data.orderNo;
                                treeObj.updateNode(treeNodes[0]);
                            } else {
                                var moveNo = treeNodes[0].orderNo;
                                var targetNo = targetNode.orderNo;
                                treeNodes[0].orderNo = targetNo;
                                targetNode.orderNo = moveNo;
                                treeObj.updateNode(treeNodes[0]);
                                treeObj.updateNode(targetNode);
                            }
                            MprogressEnd();
                        });
                    }
                });
            };

            /**
             * 菜单后面添加按钮
             */
            function addDiyDom(treeId, treeNode) {
                //获取当前节点对象
                var aObj = $("#" + treeNode.tId + "_a");
                console.log(treeNode);
                //添加删除按钮
                if ($("#del_" + treeNode.menuId).length > 0) return;
                var delStr = "<span tagId='delTag' class='span-button' style='' id='del_" + treeNode.menuId + "'>删除</span>";
                aObj.after(delStr);
                var delBtn = $("#del_" + treeNode.menuId);
                if (delBtn) delBtn.bind("click", function () {
                    //如果删除的是按钮
                    if (treeNode.isButton === 1) {
                        //弹出确认框
                        <#if theme == "经典后台Thymleaf版">
                        $.MsgBox.Confirm("温馨提示", "执行删除后将无法恢复，确定继续吗？"
                        <#else>
                        layer.confirm("<em style='color:black'>" + '执行删除后将无法恢复，确定继续吗？' + "</em>", {
                                icon: 3,
                                offset: "200px",
                                title: '温馨提示'
                            }
                            </#if>
                            , function (<#if theme == "前后端分离响应式">index</#if>) {
                                $z.ajaxPost({
                                    url: basePath + "<#if isCloudModel>/system-service</#if>/cmSysButton/delete",
                                    data: {
                                        buttonId: treeNode.menuId,
                                        canDel: treeNode.canDel
                                    },
                                    beforeSend: function () {
                                        InitMprogress();
                                    },
                                    success: function (data) {
                                        $z.dealCommonResult(data, function () {
                                            //同步删除树节点
                                            var treeObj = $.fn.zTree.getZTreeObj("ztree");
                                            treeObj.removeNode(treeNode);
                                            treeObj.removeChildNodes(treeNode);
                                            MprogressEnd();
                                        });
                                    }
                                });
                                <#if theme == "前后端分离响应式">
                                layer.close(index);
                                </#if>
                            });
                        //如果删除的是菜单
                    } else {
                        //弹出确认框
                        <#if theme == "经典后台Thymleaf版">
                        $.MsgBox.Confirm("温馨提示", "执行删除后将同步删除其子菜单和相关按钮，确定继续吗？"
                        <#else>
                        layer.confirm("<em style='color:black'>" + '执行删除后将同步删除其子菜单和相关按钮，确定继续吗？' + "</em>", {
                                icon: 3,
                                offset: "200px",
                                title: '温馨提示'
                            }
                            </#if>
                            , function (<#if theme == "前后端分离响应式">index</#if>) {
                                $z.ajaxPost({
                                    url:  basePath + "/" + controllerPrefix + "/delete",
                                    data: {
                                        menuId: treeNode.menuId,
                                        canDel: treeNode.canDel
                                    },
                                    beforeSend: function () {
                                        InitMprogress();
                                    },
                                    success: function (data) {
                                        $z.dealCommonResult(data, function () {
                                            //同步删除树节点
                                            var treeObj = $.fn.zTree.getZTreeObj("ztree");
                                            treeObj.removeNode(treeNode);
                                            treeObj.removeChildNodes(treeNode);
                                            MprogressEnd();
                                        });
                                    }
                                });
                                <#if theme == "前后端分离响应式">
                                layer.close(index);
                                </#if>
                            });
                    }
                });
                //如果是二级菜单
                if (treeNode.level === 1) {
                    //二级菜单可以添加操作节点
                    var addButtonStr = "<span tagId='addButtonTag' class='span-button' style='' id='addButton_" + treeNode.menuId + "'>添加操作</span>";
                    aObj.after(addButtonStr);
                    var addOperation = $("#addButton_" + treeNode.menuId);
                    if (addOperation) addOperation.bind("click", function () {
                        //添加操作
                        addButton(treeNode.menuId, treeNode);
                    });
                }
                //如果是操作节点
                if (treeNode.level == 2) {
                    return;
                }
                //二级菜单不能添加二级菜单
                if (treeNode.level == 1) {
                    return;
                }
                if ($("#add_" + treeNode.menuId).length > 0) return;
                var addStr = "<span tagId='addSecondMenuTag' class='span-button' id='add_" + treeNode.menuId + "'>添加二级菜单</span>";
                aObj.after(addStr);
                var addBtn = $("#add_" + treeNode.menuId);
                if (addBtn) addBtn.bind("click", function () {
                    addSecondMenu(treeNode.menuId, treeNode);
                });
            }

            /**
             * 添加一级菜单
             */
            function addFirstMenu() {
                //显示添加模态框
                $("#urlAddress-insert-group").hide();
                $("#buttonName-insert-group").hide();
                $("#buttonTagId-insert-group").hide();
                $("#menuName-insert-group").show();
                $("#menuIcon-insert-group").show();
                $("#menuName-insert").val('');
                $("#menuIcon-insert").val('');
                $('#addModal').modal('show');
                isAddButton = false;
                nowParentId = 0;
                nowParentNode = null;
            }

            /**
             * 添加二级菜单
             */
            function addSecondMenu(parentId, treeNode) {
                //显示添加模态框
                $("#buttonName-insert-group").hide();
                $("#buttonTagId-insert-group").hide();
                $("#menuIcon-insert-group").hide();
                $("#urlAddress-insert-group").show();
                $("#menuName-insert-group").show();
                $("#urlAddress-insert").val('');
                $("#menuName-insert").val('');
                //更新当前的parentId
                nowParentId = parentId;
                nowParentNode = treeNode;
                $('#addModal').modal('show');
                isAddButton = false;
            }

            /**
             * 添加操作按钮
             */
            function addButton(parentId, treeNode) {
                //显示添加模态框
                $("#menuIcon-insert-group").hide();
                $("#urlAddress-insert-group").hide();
                $("#menuName-insert-group").hide();
                $("#buttonName-insert-group").show();
                $("#buttonTagId-insert-group").show();
                $("#buttonName-insert").val('');
                $("#buttonTagId-insert").val('');
                //更新当前的parentId
                nowParentId = parentId;
                nowParentNode = treeNode;
                $('#addModal').modal('show');
                isAddButton = true;
            }


            /**
             * 确认添加菜单
             */
            function confirmAddMenu() {
                //调用后台添加菜单
                var url = isAddButton ? basePath + "<#if isCloudModel>/system-service</#if>/cmSysButton/add" : basePath + "/" + controllerPrefix + "/add";
                var data = isAddButton ? {
                    menuId: nowParentId,
                    buttonName: $("#buttonName-insert").val(),
                    moduleTagId: $("#buttonTagId-insert").val()
                } : {
                    parentId: nowParentId,
                    menuName: $("#menuName-insert").val(),
                    menuIcon: $("#menuIcon-insert").val(),
                    urlAddress: $("#urlAddress-insert").val()
                }
                $z.ajaxPost({
                    url: url,
                    data: data,
                    beforeSend: function () {
                        InitMprogress();
                    },
                    success: function (data) {
                        $z.dealCommonResult(data, function () {
                            data = data.data;
                            //动态添加节点
                            var treeObj = $.fn.zTree.getZTreeObj("ztree");
                            treeObj.addNodes(nowParentNode, data);
                            MprogressEnd();
                            $('#addModal').modal('hide');
                        });
                    }
                });
            }

            /**
             * 节点被点击时触发的事件
             */
            function zTreeOnClick(event, treeId, treeNode) {
                //判断点击的是否是按钮节点显示不同的详情
                //如果是一级菜单
                if (treeNode.level === 0) {
                    $("#menuName-update").val(treeNode.name);
                    $("#menuIcon-update").val(treeNode.menuIcon);
                    $("#menuIcon-update-group").show();
                    $("#menuName-update-group").show();
                    $("#urlAddress-update-group").hide();
                    $("#buttonName-update-group").hide();
                    $("#buttonTagId-update-group").hide();
                    //如果是二级菜单
                } else if (treeNode.level === 1) {
                    $("#menuName-update").val(treeNode.name);
                    $("#urlAddress-update").val(treeNode.urlAddress);
                    $("#menuName-update-group").show();
                    $("#urlAddress-update-group").show();
                    $("#menuIcon-update-group").hide();
                    $("#buttonName-update-group").hide();
                    $("#buttonTagId-update-group").hide();
                    //操作节点
                } else {
                    $("#buttonName-update").val(treeNode.name);
                    $("#buttonTagId-update").val(treeNode.moduleTagId);
                    $("#buttonName-update-group").show();
                    $("#buttonTagId-update-group").show();
                    $("#menuName-update-group").hide();
                    $("#urlAddress-update-group").hide();
                    $("#menuIcon-update-group").hide();
                }
                nowUpTreeNode = treeNode;
                $('#updateModal').modal('show');
            }

            /**
             * 确认修改
             */
            function confirmUp() {
                //如果更新的是按钮
                if (nowUpTreeNode.level === 2) {
                    var buttonName = $("#buttonName-update").val();
                    var moduleTagId = $("#buttonTagId-update").val();
                    $z.ajaxPost({
                        url: basePath + "<#if isCloudModel>/system-service</#if>/cmSysButton/update",
                        data: {
                            buttonId: nowUpTreeNode.menuId,
                            menuId: nowUpTreeNode.parentId,
                            buttonName: buttonName,
                            moduleTagId: moduleTagId
                        },
                        beforeSend: function () {
                            InitMprogress();
                        },
                        success: function (data) {
                            $z.dealCommonResult(data, function () {
                                MprogressEnd();
                                var treeObj = $.fn.zTree.getZTreeObj("ztree");
                                var treeNode = nowUpTreeNode;
                                //賦值
                                treeNode.name = buttonName;
                                treeNode.moduleTagId = moduleTagId;
                                //同步更新节点
                                treeObj.updateNode(treeNode);
                                $('#updateModal').modal('hide');
                            });
                        }
                    });
                    //更新的是菜单
                } else {
                    var menuName = $("#menuName-update").val();
                    var menuIcon = $("#menuIcon-update").val();
                    var urlAddress = $("#urlAddress-update").val();
                    $z.ajaxPost({
                        url: basePath + "/" + controllerPrefix + "/update",
                        data: {
                            parentId: nowUpTreeNode.parentId,
                            menuId: nowUpTreeNode.menuId,
                            menuName: menuName,
                            menuIcon: menuIcon,
                            urlAddress: urlAddress
                        },
                        beforeSend: function () {
                            InitMprogress();
                        },
                        success: function (data) {
                            $z.dealCommonResult(data, function () {
                                MprogressEnd();
                                var treeObj = $.fn.zTree.getZTreeObj("ztree");
                                var treeNode = nowUpTreeNode;
                                //賦值
                                treeNode.name = menuName;
                                treeNode.menuIcon = menuIcon;
                                treeNode.urlAddress = urlAddress;
                                //同步更新节点
                                treeObj.updateNode(treeNode);
                                $('#updateModal').modal('hide');
                            });
                        }
                    })
                }
            }

        </SCRIPT>
    </HEAD>

<BODY>
<div id="body" style="display: none">
    <hr/>
    <p style="margin-left: 10px"><b>修改，删除或改变菜单顺序后，刷新浏览器页面可同步更新左侧菜单栏！</b></p>
    <button tagId='addFirstMenuTag' style="margin-left: 10px" type="button" class="btn btn-success btn-sm" onclick="addFirstMenu()">添加一级菜单</button>
    <hr/>
    <ul id="ztree" class="ztree"></ul>
    <!--添加模态框-->
    <div class="modal fade" id="addModal" tabindex="-1" role="dialog" aria-labelledby="addModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title" id="addModalLabel">添加信息</h4>
                </div>
                <div class="modal-body" id="addModalBody">
                    <form>
                        <div class="form-group" id="menuName-insert-group">
                            <label for="menuName-insert" class="control-label">菜单名称&ensp;<label style="color: red">*</label></label>
                            <input type="text" class="form-control" id="menuName-insert"/>
                        </div>
                        <div class="form-group" id="menuIcon-insert-group">
                            <label for="menuIcon-insert" class="control-label">菜单样式</label>
                            <input type="text" class="form-control" id="menuIcon-insert"/>
                        </div>
                        <div class="form-group" id="urlAddress-insert-group">
                            <label for="urlAddress-insert" class="control-label">菜单地址&ensp;<label style="color: red">*</label></label>
                            <input type="text" class="form-control" id="urlAddress-insert"/>
                        </div>
                        <div class="form-group" id="buttonName-insert-group">
                            <label for="buttonName-insert" class="control-label">操作名称&ensp;<label style="color: red">*</label></label>
                            <input type="text" class="form-control" id="buttonName-insert"/>
                        </div>
                        <div class="form-group" id="buttonTagId-insert-group">
                            <label for="buttonTagId-insert" class="control-label">操作标识&ensp;<label style="color: red">*</label></label>
                            <input type="text" class="form-control" id="buttonTagId-insert"/>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" onclick="confirmAddMenu()">确认</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
                </div>
            </div>
        </div>
    </div>
    <!-- 菜单详情保存模态框 -->
    <div class="modal fade" id="updateModal" tabindex="-1" role="dialog" aria-labelledby="updateModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title" id="updateModalLabel">查看详情</h4>
                </div>
                <div class="modal-body" id="updateModalBody">
                    <form>
                        <div class="form-group" id="menuName-update-group">
                            <label for="menuName-update" class="control-label">菜单名称&ensp;<label style="color: red">*</label></label>
                            <input type="text" class="form-control" id="menuName-update"/>
                        </div>
                        <div class="form-group" id="menuIcon-update-group">
                            <label for="menuIcon-update" class="control-label">菜单样式</label>
                            <input type="text" class="form-control" id="menuIcon-update"/>
                        </div>
                        <div class="form-group" id="urlAddress-update-group">
                            <label for="urlAddress-update" class="control-label">菜单地址&ensp;<label style="color: red">*</label></label>
                            <input type="text" class="form-control" id="urlAddress-update"/>
                        </div>
                        <div class="form-group" id="buttonName-update-group">
                            <label for="buttonName-update" class="control-label">操作名称&ensp;<label style="color: red">*</label></label>
                            <input type="text" class="form-control" id="buttonName-update"/>
                        </div>
                        <div class="form-group" id="buttonTagId-update-group">
                            <label for="buttonTagId-update" class="control-label">操作标识&ensp;<label style="color: red">*</label></label>
                            <input type="text" class="form-control" id="buttonTagId-update"/>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button tagId='upTag' type="button" class="btn btn-primary" onclick="confirmUp()">保存</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
                </div>
            </div>
        </div>
    </div>
</div>
</BODY>
</HTML>
