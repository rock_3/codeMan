package <#if isCloudModel>${cloudSysEngName}<#else>${packageName}</#if>;
<#if isCloudModel>
import feign.config.DefaultFeignConfiguration;
<#if cloudRegisteCenter=="eureka">
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
</#if>
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
</#if>
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
<#if isMutiDataSource>import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;</#if>
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication<#if isMutiDataSource>(exclude = {DataSourceAutoConfiguration.class})</#if>
@EnableAsync
@EnableScheduling
<#if isCloudModel>
<#if cloudRegisteCenter=="eureka">
@EnableEurekaClient
</#if>
@EnableDiscoveryClient
@EnableFeignClients(basePackages = "feign.clients", defaultConfiguration = DefaultFeignConfiguration.class)
</#if>
public class ${captureProjectName}Application {

	public static void main(String[] args) {
		SpringApplication.run(${captureProjectName}Application.class, args);
	}

}
