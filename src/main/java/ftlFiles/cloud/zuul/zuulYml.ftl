#网关
spring:
<#if cloudRegisteCenter=="nacos">
  cloud:
    # 集成nacos
    nacos:
      discovery:
        server-addr: 127.0.0.1:8848
        heart-beat-interval: 30 #30S发送一次心跳
        heart-beat-timeout: 90 #90S 如果没发送心跳成功，就自动下线
        weight: 1 #设置实例权重，权重越高，处理服务能力越强
        group: ${cloudSysEngName} #微服务分组
</#if>
  application:
    name: sys-zuul
<#if cloudRegisteCenter=="eureka">
eureka:
  client:
    fetch-registry: true
    register-with-eureka: true
    service-url:
      # eureka地址
      defaultZone: http://127.0.0.1:8001/eureka/
</#if>
server:
  port: 7001
zuul:
  routes:
    # 用户基础服务
    system-service:
      path: /system-service/**
      serviceId: system-service
<#list cloudServices as service>
    ${service}-service:
      path: /${service}-service/**
      serviceId: ${service}-service
</#list>
  host:
    connect-timeout-millis: 30000
    socket-timeout-millis: 30000

ribbon:
  ReadTimeout: 30000
  ConnectTimeout: 30000
  eureka:
    enabled: true

info:
  app.name: 网关zuul
  company.name: codeMan出品
  build.artifactId: @project.artifactId@
  build.version: @project.version@
