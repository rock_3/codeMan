package <#if isCloudModel>${cloudSysEngName}<#else>${packageName}</#if>.config;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
<#if !isAuthority>
import javax.servlet.http.HttpSession;
</#if>
import org.springframework.context.annotation.Configuration;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
<#if ifUseSwagger == "是">
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
</#if>
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.method.HandlerMethod;
import <#if isCloudModel>${cloudSysEngName}<#else>${packageName}</#if>.core.annotation.LoginRequired;
import java.io.PrintWriter;
import <#if isCloudModel>${cloudSysEngName}<#else>${packageName}</#if>.utils.SessionUtil;
<#if isAuthority>
import <#if isCloudModel>${cloudSysEngName}<#else>${packageName}</#if>.entity.CmSysUserEntity;
<#else>
import <#if isCloudModel>${cloudSysEngName}<#else>${packageName}</#if>.entity.User;
</#if>

@Configuration
public class MvcConfig implements WebMvcConfigurer {

<#if theme == "经典后台Thymleaf版">
	@Override
	public void addViewControllers(ViewControllerRegistry registry) {
	<#if isAuthority>
		registry.addViewController("/cm_sys_user/list").setViewName("cmSysUser/list");
		registry.addViewController("/cm_sys_menu/list").setViewName("cmSysMenu/list");
		registry.addViewController("/cm_sys_role/list").setViewName("cmSysRole/list");
	</#if>
	<#list tableNameList?keys as key>
		registry.addViewController("/${key}/list").setViewName("${key}/list");
	</#list>

<#if tablesQueryMap??>
	<#list tablesQueryMap?keys as key>
		<#assign methods = tablesQueryMap["${key}"]/>
		<#list methods?keys as methodKey>
		registry.addViewController("/${key}Muti/${methodKey}List").setViewName("${key}Muti/${methodKey}List");
		</#list>
	</#list>
</#if>

		registry.addViewController("/home").setViewName("home");
		registry.addViewController("/welcome").setViewName("welcome");
		registry.addViewController("/login").setViewName("login");
	}
</#if>
	/**
	 * 拦截器
	 */
	@Override
	public void addInterceptors(InterceptorRegistry registry) {

		registry.addInterceptor(new HandlerInterceptor() {
			@Override
			public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
					throws Exception {
				if (handler instanceof HandlerMethod) {
					HandlerMethod handlerMethod = (HandlerMethod) handler;
					LoginRequired loginRequired = handlerMethod.getMethodAnnotation(LoginRequired.class);
					if (null == loginRequired) {
						return true;
					}
					// 预请求
		            if (RequestMethod.OPTIONS.name().equals(request.getMethod())) {
						return true;
					}
				<#if isAuthority>
					CmSysUserEntity user = SessionUtil.getUser(request);
				<#else>
				    User user = SessionUtil.getUser(request);
				</#if>
					if (user == null) {
						response.setHeader("Access-Control-Allow-Origin", request.getHeader("Origin"));
		                response.setHeader("Access-Control-Allow-Methods", "*");
		                response.setHeader("Access-Control-Max-Age", "3600");
		                response.setHeader("Access-Control-Allow-Credentials", "true");
		                response.setContentType("application/json; charset=utf-8");
		                response.setCharacterEncoding("utf-8");
		                PrintWriter pw = response.getWriter();
		                pw.write("{\"code\":" + HttpServletResponse.SC_UNAUTHORIZED + ",\"status\":\"no\",\"msg\":\"无授权访问，请先登录\"}");
		                pw.flush();
		                pw.close();
		                return false;
					}
				}
				return true;

			}
		}).addPathPatterns("/**").excludePathPatterns("/login", "/register", "/login/doLogin", "/user/register",
				"/mystatic/**", "/druid/**<#if ifUseSwagger == "是">", "/swagger-resources/**", "/webjars/**", "/v2/**", "/swagger-ui.html/**</#if>");
	}

    <#if ifUseSwagger == "是">
    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("swagger-ui.html")
                .addResourceLocations("classpath:/META-INF/resources/");
        registry.addResourceHandler("/webjars/**")
                .addResourceLocations("classpath:/META-INF/resources/webjars/");
    }
    </#if>
}
