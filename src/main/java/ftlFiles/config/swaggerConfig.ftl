package <#if isCloudModel>${cloudSysEngName}<#else>${packageName}</#if>.config;

import org.springframework.context.annotation.Bean;
<#if frameWorkVal=="springBoot">
import org.springframework.context.annotation.Configuration;
import org.springframework.beans.factory.annotation.Value;
</#if>
<#if frameWorkVal=="ssm">
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
</#if>
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

<#if frameWorkVal=="springBoot">
@Configuration
</#if>
<#if frameWorkVal=="ssm">
@EnableWebMvc
</#if>
//注解开启 swagger2 功能
@EnableSwagger2
public class SwaggerConfig {

	<#if frameWorkVal=="springBoot">
    //是否开启swagger，正式环境一般是需要关闭的
    @Value("<#noparse>${</#noparse>swagger.enabled}")
    private boolean enableSwagger;
    </#if>

    @Bean
    public Docket api() {
        return new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(apiInfo())
                 <#if frameWorkVal=="springBoot">
                //是否开启 (true 开启  false隐藏。生产环境建议隐藏)
                .enable(enableSwagger)
                 </#if>
                 <#if frameWorkVal=="ssm">
                .enable(true)
                 </#if>
                .select()
                //扫描的路径包,设置basePackage会将包下的所有被@Api标记类的所有方法作为api
                .apis(RequestHandlerSelectors.basePackage("<#if isCloudModel>${cloudSysEngName}<#else>${packageName}</#if>.controller"))
                //指定路径处理PathSelectors.any()代表所有的路径
                .paths(PathSelectors.any())
                .build();
    }

    private ApiInfo apiInfo() {
        return new ApiInfoBuilder()
                //设置文档标题(API名称)
                .title("Swagger2接口文档")
                //文档描述
                .description("接口说明")
                .license("Apache 2.0")
                .licenseUrl("http://www.apache.org/licenses/LICENSE-2.0")
                //服务条款URL
                .termsOfServiceUrl("https://swagger.io/")
                //版本号
                .version("1.0")
                .build();
    }

}
